FROM adoptopenjdk/openjdk11:alpine

WORKDIR /myapp

COPY target/*.jar .

#default values can be overwritten
ARG JAVA_OPTS="-Xms256m -Xmx1024m"
ARG profile="--spring.profiles.active=h2"
ENV profile_env=${profile} 
# i need the enviroment variable during the runtime of my container, if i use ARG it will fail when i run it because ARG is only for build time of my IMAGE


# COPY run.sh .

# RUN chmod 777 run.sh
# Application H2 profile and JAVA options should be passed as environment variables to the container and not fixed values.

# ENTRYPOINT . run.sh
ENTRYPOINT  java -jar *.jar ${JAVA_OPTS} ${profile_env}