#!/bin/bash 


DATEVALUE=$(git log -1 --format="%at" | date +%Y%m%d)

HASHVALUE=$(git log -1 --format="%H" | cut -c-8)

SHADATE=${DATEVALUE}-${HASHVALUE}
echo SHADATE=$SHADATE
# export SHADATE 
#------ exporting is not working because we have 3 subshells and there's no way the parent shell sees it -- 
# please let me know if you have a solution for this 



echo app.version=$SHADATE > config.properties


# sed -i "s/<newVersion>default<\/newVersion>/<newVersion>$SHADATE<\/newVersion>/" pom.xml

#---- another way is running mvn command in my executable and removing the properties-maven-plugin ----
# mvn versions:set -DnewVersion=$SHADATE