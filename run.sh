#!/bin/bash
## this file is used as an entrypoint to build a docker image, it's not meant to run the jar file locally
##if you wanna run the jar file locally add target/ before the jar name

##***********************IMPORTANT***************** not using this approach anymore because it's not what i need to automate the docker image build
##this prompts you to enter your paramters, as a user, i can't enter them in the pipeline shell

echo "enter your JAVA OPTS and other enviroment variables.."
read -p "java -jar assignment-$(grep app.version config.properties | cut -d'=' -f2).jar" environment_variables

java -jar target/assignment-$(grep app.version config.properties | cut -d'=' -f2).jar --spring.profiles.active=h2 $environment_variables

## OR 

# environment_variables="${@}"

# java -jar assignment-$(grep app.version config.properties | cut -d'=' -f2).jar $environment_variables
