# DevOps-practices



## Several approaches to set the new version equal to your last commit date and first eight digits of commit hash: 
<br/>

 ### 1- **Using command line arguments**
 <br/>

 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A&nbsp;- &nbsp;Go to POM.xml and add the following:

     <properties>    
        <my.variable.version> default-value </my.variable.version>
     </properties>
    
    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B&nbsp;-&nbsp;Modify <
version> element:

    <version> ${my.variable.version} </version>


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C&nbsp;-&nbsp; run the following command

    mvn clean install -Dmy.variable.version=$(./version.sh)

<br/>


 ### 2- **Using git-commit-id-plugin**
 <br/>



&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A&nbsp;-&nbsp;Add the plugin inside <
plugins> element:

    <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <version>${git-commit-id-plugin.version}</version>
                <executions>
                    <execution>
                        <id>get-the-git-infos</id>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                        <phase>validate</phase>
                    </execution>
                </executions>
                <configuration>
                    <dotGitDirectory>${project.basedir}/.git</dotGitDirectory>
                    <dateFormat>yyyyMMdd</dateFormat>
                </configuration>
    </plugin> 
<br/>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B&nbsp;-&nbsp;Modify <
version> element:

    <version> ${git.commit.time}-${git.commit.id.abbrev} </version> 



<br/>

 ### 3- **Using version plugin:**

 <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A&nbsp;-&nbsp;Add the plugin inside <
plugins> element:

<br />

    <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>

                <executions>
                    <execution>
                        <id>calculate-version</id>
                        <goals>
                            <goal>set</goal>
                        </goals>
                        <phase>generate-sources</phase>
                    </execution>
                </executions>

     </plugin>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B&nbsp;-&nbsp;Run the following command 

    mvn clean install -DnewVersion=$(./version.sh)


<br />



<br />

 ### 4- **Using a combination of exec plugin, version plugin and properties-maven-plugin:**
 <br/>

 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A&nbsp;-&nbsp;Add the plugins inside <
plugins> element:

            <plugin>
                <artifactId>exec-maven-plugin</artifactId>
                <groupId>org.codehaus.mojo</groupId>
                <executions>
                    <execution>
                        <id>calculate-version</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>exec</goal>
                        </goals>
                        <configuration>
                            <executable>version.sh</executable>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

             <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>properties-maven-plugin</artifactId>
                <version>1.0-alpha-2</version>
                <executions>
                    <execution>
                        <id>read-properties</id>
                        <phase>initialize</phase>
                        <goals>
                            <goal>read-project-properties</goal>
                        </goals>
                        <configuration>
                            <files>
                                <file>${project.basedir}/config.properties</file>
                            </files>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>calculate-version</id>
                        <goals>
                            <goal>set</goal>
                        </goals>
                        <phase>generate-sources</phase>
                    </execution>
                </executions>
                <configuration>
                    <newVersion>${app.version}</newVersion>
                </configuration>
            </plugin>


<br/>


 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B&nbsp;-&nbsp;Add final name element <
finalName> after the <
plugins> element and before the closing </ build> tag

        <finalName>${project.artifactId}-${app.version}</finalName>

<br/>
<br/>
<br/>
<br/>

### 4- **Alternative way: using only exec plugin and version plugin** 

<br />

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A- Change version.sh to the following:

    #!/bin/bash 
    DATEVALUE=$(git log -1 --format="%at" | date +%Y%m%d)
    HASHVALUE=$(git log -1 --format="%H" | cut -c-8)
    SHADATE=${DATEVALUE}-${HASHVALUE}
    mvn versions:set -DnewVersion=$SHADATE

<br />

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B- Add the plugins inside the plugins element <br />

Notice here I removed the **set goal** because it's being executed in the version.sh

    <plugin>
            <artifactId>exec-maven-plugin</artifactId>
            <groupId>org.codehaus.mojo</groupId>
            <executions>
                <execution>
                    <id>calculate-version</id>
                    <phase>validate</phase>
                    <goals>
                        <goal>exec</goal>
                    </goals>
                    <configuration>
                        <executable>version.sh</executable>
                    </configuration>
                </execution>
            </executions>
        </plugin>

     <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>

                <executions>
                    <execution>
                        <id>calculate-version</id>
                        <phase>generate-sources</phase>
                    </execution>
                </executions>

        </plugin>




<br/>


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> 60481f0b8efe410b40c93ab487b89c079948a62b
